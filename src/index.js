import { parseSVG as parseSVGPathData, makeAbsolute } from 'svg-path-parser';
import curry from 'curry';
import { Point } from 'geometry';
import { DOMParser } from 'xmldom';

const errorMessages = {
	complexPathSuppliedToSplitByClosedPath: offendingPath => `Attempted to find closed paths in complex path: ${offendingPath}`,
	pathDoesntStartWithAbsoluteMoveTo: offendingPath => `Path must begin with an absolute moveto (\`M\`) command: ${offendingPath}`,
};

// arrayFrom :: ArrayLike -> Array
function arrayFrom(arraylike) {
	if (arraylike == null) {
		return [];
	}
	return Array.prototype.slice.call(arraylike);
}

const flattenArray = (array) => array.reduce((a, b) => a.concat(b), []);

// bounds :: String -> Rect?
export function bounds(svg) {
	const captureViewBox =
		/<svg[^>]*(?:viewBox=")([-\d.]+\s+)([-\d.]+\s+)([-\d.]+\s+)([-\d.]+)"[^>]*>/;
		// /<svg[^>]*(?:viewBox=")([\-\d\.]+\s+)([\-\d\.]+\s+)([\-\d\.]+\s+)([\-\d\.]+)"[^>]*>/;
	const viewBoxMatchResult = svg.match(captureViewBox);
	if (viewBoxMatchResult != null && viewBoxMatchResult.length >= 5) {
		return {
			origin: {
				x: parseFloat(viewBoxMatchResult[1]),
				y: parseFloat(viewBoxMatchResult[2]),
			},
			size: {
				x: parseFloat(viewBoxMatchResult[3]),
				y: parseFloat(viewBoxMatchResult[4]),
			},
		};
	} else {
		return undefined;
	}
}


// Note: Does not account for transforms.
// pathDataFromSVGString :: String -> [String]
export function pathDataFromSVGString(svg) {
	const parser = new DOMParser();
	const doc = parser.parseFromString(svg, 'image/svg+xml');
	return flattenArray(arrayFrom(doc.childNodes).map(pathDataFromSVGNode));
}

// pathDataFromSVGNode :: Node -> [String]
export function pathDataFromSVGNode(node) {
	if (node.tagName === 'path') {
		const d = arrayFrom(node.attributes).find(({ name }) => name === 'd');
		if (d != null) {
			return [d.nodeValue, ...flattenArray(arrayFrom(node.childNodes).map(pathDataFromSVGNode))];
		}
	}

	return flattenArray(arrayFrom(node.childNodes).map(pathDataFromSVGNode));
}

// transformPathData :: (Transform, String) -> String
// transformPathData :: Transform -> String -> String
export const transformPathData = curry(_transformPathData);

function _transformPathData(transform, pathData) {
	const commands = parseSVGPathData(pathData);
	makeAbsolute(commands);

	function transformCommand(transform, command) {
		function transformPosition({ xKey, yKey }) {
			if (xKey in command && yKey in command) {
				const transformed = Point.transform(transform, { x: command[xKey], y: command[yKey] });
				return {
					[xKey]: transformed.x,
					[yKey]: transformed.y,
				};
			} else {
				return {};
			}
		}

		function transformMagnitude({ xKey, yKey }) {
			if (xKey in command && yKey in command) {
				const transformed = Point.transformSize(transform, { x: command[xKey], y: command[yKey] });
				return {
					[xKey]: transformed.x,
					[yKey]: transformed.y,
				};
			} else {
				return {};
			}
		}

		const commandWithTransformedPositions = [
			{ xKey: 'x', yKey: 'y' },
			{ xKey: 'x1', yKey: 'y1' },
			{ xKey: 'x2', yKey: 'y2' },
		].reduce((acc, keys) => ({ ...acc, ...transformPosition(keys) }), command);
		
		const commandWithTransformedPositionsAndMagnitudes = [
			{ xKey: 'rx', yKey: 'ry' },
		].reduce((acc, keys) => ({ ...acc, ...transformMagnitude(keys) }), commandWithTransformedPositions);

		return commandWithTransformedPositionsAndMagnitudes;
	}

	const transformedCommands = commands.map(command => transformCommand(transform, command));

	return pathDataFromParsedSVGPathData(transformedCommands);
}

// Converts the path object returned from svg-path-parser back to an SVG path string.
// Taken from https://github.com/hughsk/svg-path-parser/issues/11
//
// pathDataFromParsedSVGPathData :: Object -> String
function pathDataFromParsedSVGPathData(data){
  var params = ['rx','ry','xAxisRotation','largeArc','sweep','x1','y1','x2','y2','x','y'];
  var lastCode;
  return data.map(function(cmd){
    var a = [];
    params.forEach(function(param){
      if (param in cmd) {
        var val = cmd[param]*1; // *1 for true/false values on arc
        if (a.length && val>=0) a.push(',');
        a.push(val);
      }
    });
    var result = (lastCode===cmd.code?(a[0]<0?'':','):cmd.code) + a.join('');
    lastCode=cmd.code;
    return result;
  }).join('');
}

const charAtIndexBeginsAbsoluteMoveToCommand =
	(charIndex, string) => string.charAt(charIndex) === 'M';

const charAtIndexBeginsRelativeMoveToCommand =
	(charIndex, string) => string.charAt(charIndex) === 'm';

const charAtIndexBeginsSubpath =
	(charIndex, string) => (
		charAtIndexBeginsAbsoluteMoveToCommand(charIndex, string) 
		|| charAtIndexBeginsRelativeMoveToCommand(charIndex, string)
	);

const charAtIndexBeginsClosePathCommand =
	(charIndex, string) => (
		string.charAt(charIndex) === 'z' ||
		string.charAt(charIndex) === 'Z'
	);

/*
 * Splits a complex path into an array of subpaths.
 * This is accomplished by splitting before every `moveto` command, which implicitly
 * begins a new subpath.
 * Path must begin with an absolute moveto command to establish context.
 * This function will add an absolute moveto command at the beginning of each
 * non-initial closed subpath. This is necessary to give context
 * to the subpaths.
 *
 *    splitComplexPath('M0,0 l10,0 M0,10 l5,10') => ['M0,0 l10,0', 'M0,10 l5,10']
 *    splitComplexPath('M0,0 l10,0 z l5,10 M10,10 h5') => ['M0,0 l10,0 z l15,10', 'M10,10 h5']
 *    splitComplexPath('M0,0 l10,0 z l5,10 m-5,0 h5') => ['M0,0 l10,0 z l15,10', 'M0,0 m-5,0 h5']
 *    splitComplexPath('m0,0 l10,0 z') => throws error because path does not begin with M command
 *
 * splitComplexPath :: String -> [String]
 */
export function splitComplexPath(pathData) {
	const trimmedPathData = pathData.trim();

	if (trimmedPathData.length === 0) {
		return [];
	}

	if (trimmedPathData.charAt(0) !== 'M') {
		throw new Error(errorMessages.pathDoesntStartWithAbsoluteMoveTo(pathData));
	}

	let result = [];
	let buffer = '';
	let previousPenDownCommand = null;

	for (let i = 0; i < trimmedPathData.length; i++) {
		const currentChar = trimmedPathData.charAt(i);

		if (charAtIndexBeginsSubpath(i, trimmedPathData)) {
			result.push(buffer.trim());
			buffer = "";

			if (charAtIndexBeginsAbsoluteMoveToCommand(i, trimmedPathData)) {
				// Grab and store the pen down coordinates
				previousPenDownCommand = commandStartingFromIndex(i, trimmedPathData);
			} else {
				// Subpath didn't begin with an absolute M command, which is needed for the subpath to 
				// live on its own.
				// Start the buffer with an absolute M command to the previous pen down coordinates.
				// `previousPenDownLocation` is guaranteed to be non-null, since we already checked that
				// the path begins with an absolute moveto command.
				// Add whitespace after the command.
				buffer += `${previousPenDownCommand} `;
			}
		}

		buffer += currentChar;
	}

	result.push(buffer.trim());
	result = result.filter(subpath => subpath.length > 0);

	return result;
}

// To simplify this implementation, assume a command is started with a
// single alphabetic character ([A-Za-z]).
const charAtIndexBeginsCommand = (index, string) => string.charAt(index).match(/[A-Za-z]/) != null;

// commandStartingFromIndex :: (Int, String) -> String
function commandStartingFromIndex(index, string) {
	const charAtIndexBeginsCommand = (index, string) => string.charAt(index).match(/[A-Za-z]/) != null;

	// We want to accept one command start (the start of the command we'll return).
	let encounteredCommandStart = false;

	let result = "";
	for (let i = index; i < string.length; i++) {
		if (!encounteredCommandStart) {
			if (charAtIndexBeginsCommand(i, string)) {
				encounteredCommandStart = true;
				result += string.charAt(i);
			}
		} else {
			if (charAtIndexBeginsCommand(i, string)) {
				return result;
			} else {
				result += string.charAt(i);
			}
		}
	}

	// String ended; return what was taken thus far
	return result;
}

/*
 * Splits a simple path into multiple closed subpaths.
 * Will add an absolute moveto command at the beginning of each
 * non-initial closed subpath. This is necessary to give context
 * to the subpaths.
 * Input path must be a simple path (containing only one moveto command),
 * and must begin with an absolute moveto command.
 *
 *    splitByClosedPaths("M0,0 h10 v10 z h-10 v10 z v-10 h10")
 *    => ["M0,0 h10 v10 z", "M0,0 h-10 v10 z", "M0,0 v-10 h10"]
 *    splitByClosedPaths("M0,0 h10 v10 z")
 *    => ["M0,0 h10 v10 z"]
 *    splitByClosedPaths("m0,0 h10 v10 z")
 *    => throws error because path does not begin with an M command
 *
 * splitByClosedPaths :: String -> [String]
 */
export function splitByClosedPaths(pathData) {
	const trimmedPathData = pathData.trim();

	if (trimmedPathData.length === 0) {
		return [];
	}

	if (trimmedPathData.charAt(0) !== 'M') {
		throw new Error(errorMessages.pathDoesntStartWithAbsoluteMoveTo(pathData));
	}
	
	// guaranteed to start with M command
	const initialPenDownCommand = commandStartingFromIndex(0, pathData);

	let hasEncounteredNonMovetoCommand = false;
	let result = [];
	let buffer = '';

	for (let i = 0; i < trimmedPathData.length; i++) {
		const currentChar = trimmedPathData.charAt(i);

		if (hasEncounteredNonMovetoCommand && charAtIndexBeginsSubpath(i, trimmedPathData)) {
			throw new Error(errorMessages.complexPathSuppliedToSplitByClosedPath(pathData));
		}

		if (i !== 0 && charAtIndexBeginsCommand(i, trimmedPathData) && !charAtIndexBeginsSubpath(i, trimmedPathData)) {
			hasEncounteredNonMovetoCommand = true;
		}

		if (charAtIndexBeginsClosePathCommand(i, trimmedPathData)) {
			buffer += currentChar;
			result.push(buffer.trim());

			// Keep buffer null between closed subpaths.
			buffer = null;
		} else {
			// If we're not at the beginning and we have a null buffer,
			// add context by adding the initial M command.
			if (i !== 0 && buffer == null) {
				buffer = `${initialPenDownCommand} `;
			}

			buffer += currentChar;
		}
	}

	if (buffer != null) {
		result.push(buffer.trim());
	}
	result = result.filter(subpath => subpath.length > 0);

	return result;
}

/*
 * findFirstUnclosedPath('M0,0 l10,0 z') => null
 * findFirstUnclosedPath('M0,0 l10,0 z l10,10 z') => null
 * findFirstUnclosedPath('M0,0 l10,0') => { startIndex: 0, length: 10 }
 * findFirstUnclosedPath('M0,0 l10,0 z l10,0') => { startIndex: 13, length: 5 }
 * findFirstUnclosedPath('M0,0 l10,0 z l10,0 m10,10 l0,10 z') => { startIndex: 13, length: 6 }
 *
 * findFirstUnclosedPath :: SVGString -> { startIndex :: Int, length :: Int }?
 */
export function findFirstUnclosedPath(pathData) {
	let lastPathStartIndex = null;

	for (let i = 0; i < pathData.length; i++) {
		if (lastPathStartIndex == null) {
			if (charAtIndexBeginsCommand(i, pathData) && !charAtIndexBeginsClosePathCommand(i, pathData)) {
				lastPathStartIndex = i;
			}
		} else {
			if (charAtIndexBeginsClosePathCommand(i, pathData)) {
				// fast forward to the next command
				while (!charAtIndexBeginsCommand(i + 1, pathData)) {
					i++;

					if (i >= pathData.length) {
						return null;
					}
				}

				lastPathStartIndex = null;
			} else if (i + 1 === pathData.length) {
				return { startIndex: lastPathStartIndex, length: (i + 1) - lastPathStartIndex };
			} else if (charAtIndexBeginsSubpath(i, pathData)) {
				return { startIndex: lastPathStartIndex, length: i - lastPathStartIndex };
			}
		}
	}

	// Didn't find any unclosed paths
	return null;
}
