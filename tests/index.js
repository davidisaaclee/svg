import { install as installSourceMapSupport } from 'source-map-support';
import assert from 'assert';
import { Transform } from 'geometry';
import { parseSVG, makeAbsolute } from 'svg-path-parser';
import * as R from 'ramda';
import * as SVG from '../dist';
import * as fixtures from './fixtures';

installSourceMapSupport();

const errorMessages = {
	complexPathSuppliedToSplitByClosedPath: offendingPath => `Attempted to find closed paths in complex path: ${offendingPath}`,
	pathDoesntStartWithAbsoluteMoveTo: offendingPath => `Path must begin with an absolute moveto (\`M\`) command: ${offendingPath}`,
};

function assertEquivalentPath(path1, path2) {
	return assert.deepEqual(makeAbsolute(parseSVG(path1)), makeAbsolute(parseSVG(path2)));
}

function assertEquivalentArray(leftArray, rightArray, assertIsElementEqual, canBeEquivalentIfMismatchedLength = false) {
	if (!canBeEquivalentIfMismatchedLength && (leftArray.length !== rightArray.length)) {
		assert.fail("Length mismatch between actual and expected.");
	}

	return R.zip(leftArray, rightArray)
		.map(([left, right]) => assertIsElementEqual(left, right));
}

function assertEquivalentPathList(pathList1, pathList2) {
	return assertEquivalentArray(pathList1, pathList2, assertEquivalentPath);
}

describe("SVG", () => {
	it("should be able to transform path data", () => {
		assertEquivalentPath(
			SVG.transformPathData(Transform.translation({ x: 5, y: - 1}), 'M 1 2'),
			'M6,1');
	});

	it("should be able to extract SVG path data", () => {
		const pathStrings = [
			'M 18 20 L 10 10 z',
			'M 1 2 l 0 1',
		];
		const svgString = `<svg>
			${pathStrings.map(d => `<path d="${d}" />`)}
		</svg>`;
		assertEquivalentPathList(
			SVG.pathDataFromSVGString(svgString),
			pathStrings);
	});

	describe("::splitComplexPath", () => {
		it("should handle complex unclosed paths", () => {
			assertEquivalentPathList(
				SVG.splitComplexPath("M0,0 l10,0 M0,10 l5,10"), 
				[
					"M0,0 l10,0",
					"M0,10 l5,10"
				]);
		});

		it("should throw an error on path inputs which do not begin with an absolute moveto command", () => {
			const path1 = "m0,0 l10,20";
			assert.throws(
				() => SVG.splitComplexPath(path1),
				Error,
				errorMessages.pathDoesntStartWithAbsoluteMoveTo(path1)
			);

			const path2 = "l0,0 l10,20";
			assert.throws(
				() => SVG.splitComplexPath(path2),
				Error,
				errorMessages.pathDoesntStartWithAbsoluteMoveTo(path2)
			);

			// Shouldn't throw an error if starting with whitespace
			const path3 = "  M0,0 l10,20";
			assert.doesNotThrow(() => SVG.splitComplexPath(path3));
			const path4 = "\t  M0,0 l10,20";
			assert.doesNotThrow(() => SVG.splitComplexPath(path4));
		});

		it("should handle subpaths which do not start with an absolute moveto command", () => {
			assertEquivalentPathList(
				SVG.splitComplexPath("M0,0 L10,10 Z m10,0 L0,-10 z"),
				[
					"M0,0 L10,10 Z",
					"M0,0 m10,0 L0,-10 z"
				]
			);
		});

		it("should handle complex closed paths", () => {
			assertEquivalentPathList(
				SVG.splitComplexPath("M10,10 L12,10 z h15 m100,10 l10,10 z m15,12 v12"), 
				[
					"M10,10 L12,10 z h15",
					"M10,10 m100,10 l10,10 z",
					"M10,10 m15,12 v12"
				]);
		});

		it("should handle simple paths", () => {
			assertEquivalentPathList(SVG.splitComplexPath("M0,0 l10,0"), ["M0,0 l10,0"]);
		});

		it("should handle oddly-formatted path strings", () => {
			assertEquivalentPathList(
				SVG.splitComplexPath("  M0 0 M10,0         m20, 0"),
				["M0 0", "M10,0", "M10,0 m20, 0"]);
		});

		it("should handle large path strings", () => {
			assert.doesNotThrow(() => SVG.splitComplexPath(fixtures.paisley));
			assert.equal(SVG.splitComplexPath(fixtures.paisley).length, 711);
		});
	});

	describe("::splitByClosedPaths", () => {
		it("should error on complex paths", () => {
			const path1 = "M0,0 l12,0 z m10,0 l10,0 Z m15,10 h10 v10 z";
			assert.throws(
				() => SVG.splitByClosedPaths(path1),
				Error,
				errorMessages.complexPathSuppliedToSplitByClosedPath(path1));

			const path2 = "M0,0 l12,0 m10,0 l10,0";
			assert.throws(
				() => SVG.splitByClosedPaths(path2), 
				Error,
				errorMessages.complexPathSuppliedToSplitByClosedPath(path2));
		});

		it("should collapse complex paths with no drawing instructions", () => {
			// Even though this is technically a complex path, it should not throw, 
			// since the two moveto commands can be collapsed.
			const collapseableComplexPath = "M0,0 m10,0 l12,0";
			assert.doesNotThrow(() => SVG.splitByClosedPaths(collapseableComplexPath));

			const collapseableComplexPath2 = "M0,0 m10,0 l12,0 z l2,2 z";
			assert.doesNotThrow(() => SVG.splitByClosedPaths(collapseableComplexPath2));
			assertEquivalentPathList(
				SVG.splitByClosedPaths(collapseableComplexPath2),
				[
					"M0,0 m10,0 l12,0 z",
					"M0,0 l2,2 z"
				]);

			const collapseableComplexPath3 = "M0,0 m10,0 m0,0 l12,0 z l2,2 z";
			assert.doesNotThrow(() => SVG.splitByClosedPaths(collapseableComplexPath3));
			assertEquivalentPathList(
				SVG.splitByClosedPaths(collapseableComplexPath3),
				[
					"M0,0 m10,0 m0,0 l12,0 z",
					"M0,0 l2,2 z"
				]);
		});

		it("should error on paths which don't start with an absolute moveto command", () => {
			const path1 = "m0,0 l10,20";
			assert.throws(
				() => SVG.splitByClosedPaths(path1),
				Error,
				errorMessages.pathDoesntStartWithAbsoluteMoveTo(path1)
			);

			const path2 = "l0,0 l10,20";
			assert.throws(
				() => SVG.splitByClosedPaths(path2),
				Error,
				errorMessages.pathDoesntStartWithAbsoluteMoveTo(path2)
			);

			// Shouldn't throw an error if starting with whitespace
			const path3 = "  M0,0 l10,20";
			assert.doesNotThrow(() => SVG.splitByClosedPaths(path3));
			const path4 = "\t  M0,0 l10,20";
			assert.doesNotThrow(() => SVG.splitByClosedPaths(path4));
		});

		it("should handle simple closed paths", () => {
			assertEquivalentPathList(
				SVG.splitByClosedPaths("M0,0 l12,0 z"),
				["M0,0 l12,0 z"]);
		});

		it("should handle simple paths with multiple closings", () => {
			assertEquivalentPathList(
				SVG.splitByClosedPaths("M0,0 l12,0 z l0,10 z"),
				["M0,0 l12,0 z", "M0,0 l0,10 z"]);
		});

		it("should handle simple unclosed paths", () => {
			assert.deepEqual(SVG.splitByClosedPaths("M0,0 l12,0"), ["M0,0 l12,0"]);
		});

		it("should handle large path lists", () => {
			assert.doesNotThrow(() => SVG.splitComplexPath(fixtures.paisley).map(SVG.splitByClosedPaths));
		});
	});

	describe("::findFirstUnclosedPath", () => {
		it("should not give false positives", () => {
			assert.deepEqual(SVG.findFirstUnclosedPath('M0,0 l10,0 z'), null);
			assert.deepEqual(SVG.findFirstUnclosedPath('M0,0 l10,0 z l10,10 z'), null);
		});

		it("should detect an unclosed path", () => {
			assert.deepEqual(SVG.findFirstUnclosedPath('M0,0 l10,0'), { startIndex: 0, length: 10 });
		});

		it("should detect an unclosed path mixed with closed paths", () => {
			assert.deepEqual(SVG.findFirstUnclosedPath('M0,0 l10,0 z l10,0'), { startIndex: 13, length: 5 });
			assert.deepEqual(SVG.findFirstUnclosedPath('M0,0 l10,0 z l10,0 m10,10 l0,10 z'), { startIndex: 13, length: 6 });
			assert.deepEqual(SVG.findFirstUnclosedPath('M0,0l10,0zl10,0m10,10l0,10 z'), { startIndex: 10, length: 5 });
		});
	});
});

